# Change Log

## v2.0-beta

-   Web UI 升级

## v1.2-beta

-   优化 BASE 镜像，首次更新可能耗时比较久；解决了部分机型因为环境安装不上问题
-   支持 HA Core 版本，可通过 docker 方式直接安装。
-   支持 HA 的 URL 设置

## v1.1-beta

-   多通道设备自动生成 Lovelace
-   支持 DIY 模式的设备
-   支持 HA 的端口设置

## v1.0-beta

-   支持单通道开关插座
-   支持多通道开关插座
-   支持 RGB 灯带
-   支持双色冷暖灯泡
